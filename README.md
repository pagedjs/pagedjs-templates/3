---
author: Alice Ricci
title: Dauendorf
project: paged.js sprint
book format: letter
book orientation: portrait
---



## Typefaces
### Fivo Sans Modern
Design by Alex Slobzheninov
https://www.behance.net/gallery/54442585/FIvo-Sans-Modern-Free-Display-Typeface
SIL Open Font License




## Supported tags
See issue
